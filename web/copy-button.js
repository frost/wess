function copy(event) {
	let button = event.target
	let container = event.target.parentElement
	let pre = container.querySelector("pre")
	let text = pre.textContent
	console.log("Copying text:", text)
	navigator.clipboard.writeText(text)
	
	button.classList.add("flash")
	let removeAnimation = function (event) {
		// remove the flash animation class again
		if (event.animationName == "copy-flash") {
			button.classList.remove("flash")
		}
		button.removeEventListener("animationend", removeAnimation)
	}
	button.addEventListener("animationend", removeAnimation)
	
	let oldContent = button.textContent
	button.textContent = "Copied!"
	window.setTimeout(() => button.textContent = oldContent, 1000)
}

function setupCopyButtons() {
	let buttons = document.querySelectorAll(".copy-button")
	for (button of buttons) {
		console.log("Setting up copy button", button)
		button.addEventListener("click", copy)
	}
}

setupCopyButtons()
