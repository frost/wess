//
// wess-web.js
// wess
//
// Created by Frost on 2022-01-08
//
// This is free and unencumbered software released into the public domain.
// See COPYING.md for more information.
//

let wordLength = 5
let maxGuesses = 6

let currentGuess = 1
let currentWord = ""

let wess = new Wess()

Date.prototype.formatISODateOnly = function() {
	return `${this.getFullYear()}-${String(this.getMonth()+1).padStart(2, 0)}-${String(this.getDate()).padStart(2, 0)}`
}

function setupGrid(table, maxWords) {
	console.log("Setting up grid.")
	let rows = []
	for (let i = 0; i < maxWords; i++) {
		let row = document.createElement("tr")
		for (let j = 0; j < wordLength; j++) {
			cell = document.createElement("td")
			row.appendChild(cell)
		}
		rows.push(row)
	}
	table.replaceChildren(...rows)
}
function setupMainGrid() {
	setupGrid(document.getElementById("wess-grid"), maxGuesses)
}
function setupInputField() {
	let input = document.getElementById("wess-input")
	input.addEventListener("input", updateWord)
	// input.addEventListener("change", makeGuess)
	input.addEventListener("keydown", (event) => {
		if (event.key == "Enter") {
			makeGuess()
		}
	})
	input.focus()
	// clicking on the background focuses the input again
	document.body.addEventListener("click", refocusInputField)
	
	// clear out any previous value from before page reload
	input.value = ""
}
function refocusInputField(event) {
	console.log("Refocusing input field")
	let input = document.getElementById("wess-input")
	input.focus()
}
function setupFocusButtons(dateString) {
	let prevButton = document.getElementById("prev-puzzle-button")
	let nextButton = document.getElementById("next-puzzle-button")
	
	let now = new Date(dateString + " 00:00:00")
	let yesterday = new Date(now.getTime())
	yesterday.setDate(now.getDate() - 1) // this DOES in fact spill over month boundaries correctly! Weird.
	let tomorrow = new Date(now.getTime())
	tomorrow.setDate(now.getDate() + 1) // ditto
	
	console.log("yesterday:", yesterday)
	console.log("today:    ", now)
	console.log("tomorrow: ", tomorrow)
	
	let formattedYesterday = yesterday.formatISODateOnly()
	let formattedTomorrow  = tomorrow.formatISODateOnly()
	if (wess.checkDate(formattedYesterday)) {
		prevButton.href = "?date=" + formattedYesterday
	} else {
		prevButton.removeAttribute("href")
	}
	if (wess.checkDate(formattedTomorrow)) {
		nextButton.href = "?date=" + formattedTomorrow
	} else {
		nextButton.removeAttribute("href")
	}
}
function hideResultsContainer() {
	let results = document.getElementById("results-container")
	results.style.display = "none"
}

function updateWord(event) {
	// limit to 5 letters
	let word = event.target.value
	word = word.replaceAll(/[^a-zA-Z]/g, "").substring(0, wordLength)
	word = word.toLowerCase()
	currentWord = word
	event.target.value = word
	
	let table = document.getElementById("wess-grid")
	displayWord(table, word, currentGuess)
	showMatches(currentGuess, null, true, word.split(""))
}
function displayWord(table, word, index) {
	let row = table.children[index - 1]
	for (let i = 0; i < row.children.length; i++) {
		let cell = row.children[i]
		if (i < word.length) {
			cell.textContent = word[i]
		} else {
			cell.textContent = null
		}
	}
}
function showMatches(currentGuess, matches, pending, pendingChars) {
	let table = document.getElementById("wess-grid")
	let row = table.children[currentGuess - 1]
	for (let i = 0; i < row.children.length; i++) {
		let cell = row.children[i]
		// clear any cruft that got added during typing
		if (pending) {
			cell.classList.add("pending")
		} else {
			cell.classList.remove("pending")
		}
		cell.classList.remove("correct")
		cell.classList.remove("wrongpos")
		cell.classList.remove("incorrect")
		let match = null
		if (pending) {
			match = wess.previouslyGuessedLetters()[pendingChars[i]]
		} else {
			match = matches[i]
		}
		switch (match) {
			case Match.correct:
				cell.classList.add("correct")
				break
			case Match.wrongPosition:
				cell.classList.add("wrongpos")
				break
			case Match.incorrect:
				cell.classList.add("incorrect")
				break
		}
	}
}

function makeGuess() {
	console.log("Enter:", currentWord)
	if (currentWord.length != wordLength) {
		return
	}
	let matches = []
	try {
		matches = wess.makeGuess(currentWord)
	} catch (error) {
		console.log("Guess failed:", error)
		let table = document.getElementById("wess-grid")
		let row = table.children[currentGuess - 1]
		row.classList.add("flash")
		let removeAnimation = function (event) {
			// remove the flash animation class again
			if (event.animationName == "flash") {
				row.classList.remove("flash")
			}
			row.removeEventListener("animationend", removeAnimation)
		}
		row.addEventListener("animationend", removeAnimation)
		return
	}
	console.log(matches)
	
	showMatches(currentGuess, matches)
	
	if (currentWord == wess.getWord()) {
		finish(true)
		return
	}
	
	currentGuess++
	currentWord = ""
	document.getElementById("wess-input").value = ""
	
	if (currentGuess > maxGuesses) {
		finish(false)
	}
}

async function finish(success) {
	console.log("Game end:", (success) ? "won" : "lost")
	
	let input = document.getElementById("wess-input")
	input.blur()
	document.body.removeEventListener("click", refocusInputField)
	
	// let gameContainer = document.getElementById("wess-container")
	// gameContainer.style.display = "none"
	
	let results = document.getElementById("results-container")
	results.style.display = null
	results.style.visibility = "visible"
	
	let heading = document.getElementById("results-heading")
	heading.textContent = (success) ? "You won!" : "You lost."
	
	let resultsGrid = document.getElementById("results-grid")
	setupGrid(resultsGrid, 1)
	displayWord(resultsGrid, wess.getWord(), 1)
	
	let shareText = document.getElementById("share-text")
	let versionInfo = await (await fetch("version.txt")).text()
	shareText.textContent = wess.generateShareCode(versionInfo, currentGuess, maxGuesses)
	
	results.scrollIntoView(true, {behavior: "smooth"})
}

async function generateWord() {
	let now = new Date()
	let formattedDate = now.formatISODateOnly()
	let seed = formattedDate
	
	let queryString = new URLSearchParams(window.location.search)
	let providedDate = queryString.get("date")
	if (providedDate) {
		console.log("Checking user-provided date:", providedDate)
		try {
			if (wess.checkDate(providedDate)) {
				seed = providedDate
			} else {
				displayError("Date can't be in the future - no cheating!")
				return
			}
		} catch (error) {
			displayError(error.message)
			return
		}
	}
	
	let response = await fetch("words.txt")
	let words = (await response.text()).split("\n")
	console.log("Seed:", seed)
	wess.generateWord(seed, words)
	
	let dateIndicator = document.getElementById("date")
	dateIndicator.textContent = seed
	
	return seed
}

function displayError(message) {
	let gameContainer = document.getElementById("wess-container")
	gameContainer.style.display = "none"
	
	let results = document.getElementById("results-container")
	
	let heading = document.getElementById("results-heading")
	heading.textContent = "Error"
	
	document.getElementById("results-grid-title").style.display = "none"
	document.getElementById("results-grid").style.display = "none"
	document.getElementById("share-code-container").style.display = "none"
	
	let resultsText = document.getElementById("results-text")
	resultsText.textContent = message
	
	results.style.display = null
}

hideResultsContainer()
setupMainGrid()
generateWord().then((date) => setupFocusButtons(date))
setupInputField()
