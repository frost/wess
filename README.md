# Wess

This is a small Wordle clone that lets you go back and do puzzles from previous days. So you don't have to worry about missing them!

### Dependencies

- npm seedrandom package

Just run `npm install` and you should be good to go.

### Usage

Either run `wess` from this directory, or start up a web server inside `web/`.

### License

This is free and unencumbered software released into the public domain.
See COPYING.md for more information.
