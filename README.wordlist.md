### Regenerating the wordlist version file

If you edit the wordlist, you should regenerate the version file so other critters can compare their results properly.

The wordlist version is the first 8 bytes of its SHA-256 hash.

```
sha256sum words.txt | head -c 8 > version.txt
```
