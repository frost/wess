#!/bin/sh
cat words.txt | sort | uniq > words.txt.new
cat words.txt.new > words.txt
rm words.txt.new
sha256sum words.txt | head -c 8 > version.txt
