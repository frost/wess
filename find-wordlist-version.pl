#!/usr/bin/env perl

use strict;
use warnings;
use v5.10;

my @hashes = split "\n", `git rev-list HEAD`;
# my @changedhashes = split "\n", `git rev-list HEAD version.txt`;

my $oldversionstring = "";
for my $hash (@hashes) {
	my $versionstring = `git show $hash:version.txt 2> /dev/null`;
	chomp $versionstring;
	if ($versionstring eq "") {
		## no version string in this commit!
		next;
		## assume all commits after this one don't have the file either
		# last;
	}
	if ($versionstring ne $oldversionstring) {
		printf "wordlist version %s: commit %s\n", $versionstring, $hash;
	} else {
		# printf "\e[2m%s not modified [%s]\e[0m\n", $hash, $versionstring;
	}
	$oldversionstring = $versionstring;
}
