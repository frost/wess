//
// wess-core.js
// wess
//
// Created by Frost on 2022-01-07
//
// This is free and unencumbered software released into the public domain.
// See COPYING.md for more information.
//

let Match = {
	incorrect: 0,
	wrongPosition: 1,
	correct: 2
}

class Wess {
	#seed = null
	#todaysWord = ""
	#wordlist = []
	#previouslyGuessedLetters = {}
	#matchHistory = []
	
	generateWord(seed, wordlist) {
		this.#seed = seed
		this.#wordlist = wordlist
		let rng = new Wess.seedrandom(seed)
		let normalized = rng()
		let index = Math.floor(normalized * wordlist.length)
		this.#todaysWord = wordlist[index]
	}
	getWord() {
		return this.#todaysWord
	}
	/* ONLY FOR TESTING PURPOSES */
	setWord(word) {
		this.#todaysWord = word
	}
	
	makeGuess(word) {
		if (word.length != this.#todaysWord.length) {
			throw new Error("Word is the wrong length")
		}
		if (! this.#wordlist.some((w) => w === word)) {
			throw new Error("Not in wordlist")
		}
		// find matches
		let matches = Array(word.length)
		for (let i = 0; i < word.length; i++) {
			if (word[i] === this.#todaysWord[i]) {
				matches[i] = Match.correct
			} else if (this.#todaysWord.split("").some((c) => {
				// console.log(`comparing ${c} and ${word[i]}`)
				return c === word[i]
			})) {
				matches[i] = Match.wrongPosition
			} else {
				matches[i] = Match.incorrect
			}
		}
		for (let i = 0; i < matches.length; i++) {
			// add this to the 'virtual keyboard' letter feedback
			this.#previouslyGuessedLetters[word[i]] = matches[i]
		}
		this.#matchHistory.push(matches)
		return matches
	}
	
	previouslyGuessedLetters() {
		return this.#previouslyGuessedLetters
	}
	
	generateShareCode(version, guesses, maxGuesses) {
		let shareCode = this.#matchHistory.map((match) => {
			return match.map((x) => {
				switch (x) {
					case Match.correct:
						return "🟩"
					case Match.wrongPosition:
						return "🟨"
					case Match.incorrect:
						return "⬛"
					default:
						return "❔"
				}
			}).join("")
		})
		if (guesses > maxGuesses) {
			guesses = "X"
		}
		shareCode = [
			`Wess v.${version}: ${this.#seed} ${guesses}/${maxGuesses}`,
			"",
			...shareCode
		]
		return shareCode.join("\n")
	}
	
	checkDate(dateString) {
		if (/^\d\d\d\d-\d\d-\d\d$/.test(dateString) == false) {
			throw new Error("Date needs to be in YYYY-MM-DD format")
		}
		let now = new Date()
		let date = new Date(dateString + " 00:00:00")
		if (isNaN(date)) {
			throw new Error("Date parsing failed")
		}
		// console.log(date)
		// console.log(now)
		if (date > now) {
			return false
		}
		return true
	}
}

if (typeof require === "undefined") {
	// we're running in the browser
	Wess.seedrandom = Math.seedrandom
} else {
	Wess.seedrandom = require("seedrandom")
}

// export * as wess
if (typeof module !== "undefined") {
	module.exports = { Wess, Match }
}
